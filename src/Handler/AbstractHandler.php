<?php
/**
 * AbstractHandler
 *
 * Created at 2019-06-30 11:39 PM
 *
 * @author Serghei Niculaev <spam312sn@gmail.com>
 * @license GNU GPLv3 <https://www.gnu.org/licenses/gpl-3.0.en.html>
 *
 * This file is a part of éNuage version updater command
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

namespace Enuage\VersionUpdaterBundle\Handler;

use Enuage\Type\StringObject;
use Enuage\VersionUpdaterBundle\Formatter\FormatterInterface;
use Enuage\VersionUpdaterBundle\Helper\Type\FileType;
use Enuage\VersionUpdaterBundle\Parser\AbstractParser;
use Enuage\VersionUpdaterBundle\Parser\FileParser;

/**
 * Class AbstractHandler
 *
 * @author Serghei Niculaev <spam312sn@gmail.com>
 */
abstract class AbstractHandler
{
    /** @var StringObject */
    protected $pattern;

    /** @var FileParser */
    private $parser;

    abstract public function handle(FormatterInterface $formatter): string;

    public function getFileContent(): string {
        return $this->getParser()->getFile()->getContents();
    }

    public function getPattern(): string
    {
        return sprintf('/%s/', AbstractParser::VERSION_PATTERN);
    }

    public function setPattern(string $pattern): void
    {
        $this->pattern = new StringObject($pattern);
    }

    public function setParser(FileParser $parser): AbstractHandler
    {
        $this->parser = $parser;

        return $this;
    }

    protected function getParser(): FileParser
    {
        return $this->parser;
    }

    public static function getHandlerByFileType(string $fileType): self
    {
        switch ($fileType) {
            case FileType::TYPE_JSON:
                return new JsonHandler();
            case FileType::TYPE_JSON_COMPOSER:
                return new ComposerHandler();
            case FileType::TYPE_YAML:
                return new YamlHandler();
            default:
                return new TextHandler();
        }
    }
}
