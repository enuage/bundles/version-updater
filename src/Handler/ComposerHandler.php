<?php

namespace Enuage\VersionUpdaterBundle\Handler;

use Enuage\VersionUpdaterBundle\Exception\InvalidFileException;
use Enuage\VersionUpdaterBundle\Formatter\FormatterInterface;
use Enuage\VersionUpdaterBundle\Formatter\VersionFormatter;
use Exception;

/**
 * Class ComposerHandler
 *
 * @author Serghei Niculaev <s.niculaev@dynatech.lv>
 */
final class ComposerHandler extends JsonHandler
{
    public const FILENAME = 'composer.json';

    // https://getcomposer.org/doc/04-schema.md#version
    private const VERSION_PROPERTY = 'version';

    public function __construct() {
        $this->setPattern(self::VERSION_PROPERTY);
    }

    /**
     * @param VersionFormatter $formatter
     *
     * @throws Exception
     */
    public function handle(FormatterInterface $formatter): string
    {
        $formatter = clone $formatter;

        // Composer don't understand suffixes and meta tags
        $formatter->updateBaseVersionOnly();
        $formatter->disablePrefix();

        return parent::handle($formatter);
    }

    /**
     * @throws Exception
     */
    public function decodeContent(string $content): array
    {
        $result = parent::decodeContent($content);

        if (
            empty($result)
            || !array_key_exists(self::VERSION_PROPERTY, $result)
            || empty($result[self::VERSION_PROPERTY])
        ) {
            $file = $this->getParser()->getFile();
            throw InvalidFileException::versionNotFound($file->getFilename());
        }

        return $result;
    }
}
