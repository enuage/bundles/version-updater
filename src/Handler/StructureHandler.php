<?php

namespace Enuage\VersionUpdaterBundle\Handler;

use Closure;
use Enuage\Type\AdvancedArrayObject;
use Enuage\VersionUpdaterBundle\Formatter\FormatterInterface;
use Exception;

/**
 * Class StructureHandler
 *
 * @author Serghei Niculaev <s.niculaev@dynatech.lv>
 */
abstract class StructureHandler extends AbstractHandler
{
    abstract public static function getExtensions(): array;

    /**
     * @throws Exception
     */
    public function getFileContent(): string
    {
        return $this->getValue($this->decodeContent(parent::getFileContent()));
    }

    /**
     * @throws Exception
     */
    private function getValue(array $content)
    {
        $this->accessProperty(
            $content,
            static function ($property) use (&$value) {
                $value = $property;
            }
        );

        return $value;
    }

    /**
     * @throws Exception
     */
    protected function updateProperty(FormatterInterface $formatter): array
    {
        $content = $this->decodeContent(parent::getFileContent());

        $this->accessProperty(
            $content,
            static function (&$property) use ($formatter) {
                $property = $formatter->format();
            }
        );

        return $content;
    }

    abstract public function decodeContent(string $content): array;

    /**
     * @throws Exception
     */
    private function accessProperty(
        array &$content,
        Closure $closure,
        AdvancedArrayObject $properties = null
    ): void {
        if (null === $properties) {
            $properties = $this->pattern->explode('/');
        }

        foreach ($properties->getIterator() as $index => $property) {
            if (array_key_exists($property, $content)) {
                $propertyValue = &$content[$property];

                if (is_array($propertyValue)) {
                    $properties->removeByKey($index);

                    $this->accessProperty(
                        $propertyValue,
                        $closure,
                        $properties
                    );
                }

                if (is_string($propertyValue)) {
                    $closure($propertyValue);
                }
            }
        }
    }
}
