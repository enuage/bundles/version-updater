<?php

namespace Enuage\VersionUpdaterBundle\Finder;

use Doctrine\Common\Collections\Collection;
use Enuage\Type\AdvancedArrayObject;
use Enuage\Type\Throwable\InvalidTypeException;
use Enuage\VersionUpdaterBundle\Exception\VersionFinderException;
use Enuage\VersionUpdaterBundle\Helper\Type\FileType;
use Enuage\VersionUpdaterBundle\Parser\GitParser;
use Enuage\VersionUpdaterBundle\Service\VersionService;
use Exception;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * Class VersionFinder
 *
 * @author Serghei Niculaev <s.niculaev@dynatech.lv>
 */
class VersionFinder
{
    public const SOURCE_COMPOSER = 'composer';
    public const SOURCE_GIT = 'git';

    /** @var Collection */
    private $sources;

    /** @var VersionService */
    private $versionService;

    public function __construct()
    {
        $this->sources = new AdvancedArrayObject();
        $this->versionService = new VersionService();
    }

    /**
     * @throws VersionFinderException
     * @throws InvalidTypeException
     */
    public function findAll(): self
    {
        $this->getComposerVersion(true);
        $this->getGitVersion(true);

        return $this;
    }

    /**
     * @throws VersionFinderException
     * @throws InvalidTypeException
     * @throws Exception
     */
    public function getComposerVersion(bool $silent = false): string
    {
        $filePath = sprintf('%s/composer.json', getcwd());

        $version = 'No data';
        if (!file_exists($filePath)) {
            if (false === $silent) {
                throw VersionFinderException::composerNotFound();
            }
        } else {
            $version = $this->versionService->getVersionFromFile(
                $filePath,
                FileType::TYPE_JSON_COMPOSER
            );

            $this->addSource(self::SOURCE_COMPOSER, $version);
        }

        return $version;
    }

    /**
     * @throws InvalidTypeException
     */
    private function addSource(string $type, string $version): void
    {
        $this->sources->add(new AdvancedArrayObject([
            'type' => $type,
            'version' => $version,
        ]));
    }

    /**
     * @throws VersionFinderException
     * @throws InvalidTypeException
     */
    public function getGitVersion(bool $silent = false): string
    {
        $version = 'No data';
        try {
            $gitParser = new GitParser();
            $gitParser->check();

            $version = $this->versionService->getVersionFromGit();

            $this->addSource(self::SOURCE_GIT, $version);
        } catch (VersionFinderException $exception) {
            if (false === $silent) {
                throw VersionFinderException::gitNotFound();
            }
        }

        return $version;
    }

    /**
     * @throws Exception
     */
    public function cliOutput(SymfonyStyle $io): void
    {
        if (!$this->sources->isEmpty()) {
            /** @var Collection $source */
            foreach ($this->sources->getIterator() as $source) {
                $io->writeln($source->get('type').': '.$source->get('version'));
            }
        }
    }
}
