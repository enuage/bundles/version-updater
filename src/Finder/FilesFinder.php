<?php
/**
 * FilesFinder
 *
 * Created at 2019-06-23 1:56 PM
 *
 * @author Serghei Niculaev <spam312sn@gmail.com>
 * @license GNU GPLv3 <https://www.gnu.org/licenses/gpl-3.0.en.html>
 *
 * This file is a part of éNuage version updater command
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

namespace Enuage\VersionUpdaterBundle\Finder;

use Closure;
use Enuage\Type\AdvancedArrayObject;
use Enuage\Type\StringObject;
use Enuage\VersionUpdaterBundle\DependencyInjection\Configuration;
use Enuage\VersionUpdaterBundle\Exception\FileNotFoundException;
use Enuage\VersionUpdaterBundle\Exception\InvalidFileException;
use Enuage\VersionUpdaterBundle\Normalizer\FilesArrayNormalizer;
use Exception;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Finder\SplFileInfo;

/**
 * Class FilesFinder
 *
 * @author Serghei Niculaev <spam312sn@gmail.com>
 */
class FilesFinder
{
    /** @var AdvancedArrayObject */
    private $files;

    /** @var string */
    private $rootDirectory;

    /** @var AdvancedArrayObject */
    private $extensions;

    public function __construct()
    {
        $this->rootDirectory = getcwd();
        $this->extensions = new AdvancedArrayObject();
    }

    public function setFiles(array $files): FilesFinder
    {
        $this->files = FilesArrayNormalizer::normalize($files);

        return $this;
    }

    /**
     * @throws FileNotFoundException
     * @throws InvalidFileException
     */
    public function iterate(Closure $closure): void
    {
        foreach ($this->files as $filePath => $pattern) {
            $file = $this->getFile($filePath);

            $closure($file, $pattern);
        }
    }

    /**
     * @throws FileNotFoundException
     * @throws InvalidFileException
     * @throws Exception
     */
    public function getFile(
        string $path,
        bool $isConfiguration = false
    ): SplFileInfo {
        $pathToFile = new StringObject($path);

        $absolutePath = new AdvancedArrayObject([$this->rootDirectory]);
        $absolutePath->append($pathToFile);

        $isPathFromRoot = $this->isFromRoot($pathToFile);
        $pathToFile = $pathToFile->explode(
            DIRECTORY_SEPARATOR,
            StringObject::MODE_REMOVE_EMPTY_VALUES
        );

        if ($isPathFromRoot) {
            $absolutePath = $pathToFile;
        }

        $fileName = new StringObject(
            $isConfiguration ? Configuration::CONFIG_FILE : $pathToFile->last()
        );

        if (
            !$isConfiguration xor
            Configuration::CONFIG_FILE === $pathToFile->last()
        ) {
            $pathToFile->removeElement($pathToFile->last());
        }

        $directory = (
            $isPathFromRoot ? $absolutePath : $pathToFile
        )->implode(DIRECTORY_SEPARATOR, true);

        if (!$directory->isEndsWith(DIRECTORY_SEPARATOR)) {
            $directory->append(DIRECTORY_SEPARATOR);
        }

        if ($isPathFromRoot && !$directory->isStartsWith(DIRECTORY_SEPARATOR)) {
            $directory->prepend(DIRECTORY_SEPARATOR);
        }

        if ($directory->isEqualTo(DIRECTORY_SEPARATOR)) {
            $directory = new StringObject('.');
        }

        return $this->findFile($directory, $fileName, $this->extensions->first());
    }

    /**
     * @param StringObject $directory
     * @param StringObject $name
     * @param string|null $fileExtension
     *
     * @return SplFileInfo
     *
     * @throws FileNotFoundException
     * @throws InvalidFileException
     */
    private function findFile(
        StringObject $directory,
        StringObject $name,
        string $fileExtension = null
    ): SplFileInfo {
        if ($name->isEmpty()) {
            throw new InvalidFileException($directory, $name);
        }

        if (null !== $fileExtension) {
            $name->reset()->append('.')->append($fileExtension);
        }

        $finder = new Finder();
        $finder->files();
        $finder->in($directory->getValue());
        $finder->notPath('vendor');
        $finder->depth(0); // Restrict recursive search
        $finder->name($name->getValue());
        $finder->ignoreDotFiles(false);

        $file = AdvancedArrayObject::fromIterator(
            $finder->getIterator()
        )->first();

        if (is_bool($file) && !$this->extensions->isEmpty()) {
            $extension = $this->extensions->current();

            if (null !== $fileExtension) {
                $extension = $this->extensions->getNext($fileExtension);
            }

            if (null === $extension) {
                throw new FileNotFoundException(
                    $directory,
                    $name->getInitialValue(),
                    $this->extensions
                );
            }

            return $this->findFile($directory, $name, $extension);
        }

        if (!$finder->hasResults()) {
            throw new FileNotFoundException(
                $directory,
                $name->getInitialValue(),
                $this->extensions
            );
        }

        return $file;
    }

    public function setRootDirectory(string $rootDirectory): self
    {
        $this->rootDirectory = $rootDirectory;

        return $this;
    }

    public function hasFiles(): bool
    {
        return !$this->files->isEmpty();
    }

    public function setExtensions(array $extensions): self
    {
        $this->extensions = new AdvancedArrayObject($extensions);

        return $this;
    }

    private function isFromRoot(StringObject $path): bool
    {
        return $path->isStartsWith(DIRECTORY_SEPARATOR) ||
            $path->isStartsWith('~/');
    }
}
