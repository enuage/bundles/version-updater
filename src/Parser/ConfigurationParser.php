<?php

namespace Enuage\VersionUpdaterBundle\Parser;

use Enuage\Type\AdvancedArrayObject;
use Enuage\VersionUpdaterBundle\Exception\FileNotFoundException;
use Enuage\VersionUpdaterBundle\Exception\InvalidFileException;
use Enuage\VersionUpdaterBundle\Finder\FilesFinder;
use Enuage\VersionUpdaterBundle\Handler\YamlHandler;
use Exception;

/**
 * Class ConfigurationParser
 *
 * @author Serghei Niculaev <s.niculaev@dynatech.lv>
 */
class ConfigurationParser extends AdvancedArrayObject
{
    protected const GET_MESSAGE_COMMIT = 'commit';
    protected const GET_MESSAGE_RELEASE = 'release';

    public static function parseConfiguration(
        array $parameters
    ): ConfigurationParser {
        return new self($parameters);
    }

    /**
     * @throws FileNotFoundException
     * @throws InvalidFileException
     */
    public static function parseFile(
        string $path,
        FilesFinder $finder = null
    ): ConfigurationParser {
        if(null === $finder) {
            $finder = new FilesFinder();
        }

        $fileParser = new FileParser(
            $finder->getFile($path, true),
            new YamlHandler()
        );

        return new self($fileParser->decodeContent());
    }

    /**
     * @throws Exception
     */
    public function getFiles(string $type = 'files'): ?array
    {
        if ($this->containsKey($type)) {
            return $this->get($type);
        }

        return null;
    }

    /**
     * @throws Exception
     */
    public function isGitEnabled(): bool
    {
        if (false === $this->containsKey('git')) {
            return false;
        }

        $gitConfiguration = $this->get('git', []);
        if (array_key_exists('enabled', $gitConfiguration)) {
            return filter_var(
                $gitConfiguration['enabled'],
                FILTER_VALIDATE_BOOLEAN
            );
        }

        return true;
    }

    /**
     * @throws Exception
     */
    public function isGitPushEnabled(): bool
    {
        if ($this->isGitEnabled()) {
            $gitConfiguration = $this->getGitConfiguration();

            if (array_key_exists('push', $gitConfiguration)) {
                return filter_var(
                    $gitConfiguration['push'],
                    FILTER_VALIDATE_BOOLEAN
                );
            }
        }

        return false;
    }

    /**
     * @throws Exception
     */
    public function getGitPrefix(): string
    {
        if ($this->isGitEnabled()) {
            $gitConfiguration = $this->getGitConfiguration();

            if (array_key_exists('prefix', $gitConfiguration)) {
                return (string) $gitConfiguration['prefix'];
            }
        }

        return '';
    }

    /**
     * @throws Exception
     */
    private function getGitConfiguration(): array
    {
        return $this->get('git', []);
    }

    /**
     * @throws Exception
     */
    public function getGitCommitMessage(string $defaultMessage): string
    {
        return $this->getGitMessage(self::GET_MESSAGE_COMMIT, $defaultMessage);
    }

    /**
     * @throws Exception
     */
    public function getGitReleaseMessage(string $defaultMessage): string
    {
        return $this->getGitMessage(self::GET_MESSAGE_RELEASE, $defaultMessage);
    }

    /**
     * @throws Exception
     */
    protected function getGitMessage(
        string $type,
        string $defaultMessage
    ): string {
        if ($this->isGitEnabled()) {
            $gitConfiguration = $this->getGitConfiguration();

            $messageConfiguration = [];
            if (array_key_exists('message', $gitConfiguration)) {
                $messageConfiguration = $gitConfiguration['message'] ?? [];
            }

            if (array_key_exists($type, $messageConfiguration)) {
                if (!empty($messageConfiguration[$type])) {
                    return $messageConfiguration[$type];
                }
            }
        }

        return $defaultMessage;
    }
}
