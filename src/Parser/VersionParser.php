<?php
/**
 * VersionParser
 *
 * Created at 2019-06-23 12:24 AM
 *
 * @author Serghei Niculaev <spam312sn@gmail.com>
 * @license GNU GPLv3 <https://www.gnu.org/licenses/gpl-3.0.en.html>
 *
 * This file is a part of éNuage version updater command
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

namespace Enuage\VersionUpdaterBundle\Parser;

use Enuage\VersionUpdaterBundle\ValueObject\Version;
use Exception;

/**
 * Class VersionParser
 *
 * @author Serghei Niculaev <spam312sn@gmail.com>
 */
class VersionParser extends AbstractParser
{
    /** @var string */
    private $subject;

    public function __construct(string $subject = null)
    {
        parent::__construct();

        $this->subject = $subject;

        $this->setPattern(sprintf('/%s/', self::VERSION_PATTERN));
    }

    /**
     * @throws Exception
     */
    public function parse(): Version
    {
        $subject = $this->getSubject();
        if (is_numeric($subject) && false === strpos($subject, '.')){
            $subject = intval($subject);
        }

        preg_match($this->getPattern(), $subject, $matches);

        $this->cloneMatches($matches);

        return $this->getVersion();
    }

    private function getSubject(): string
    {
        return $this->subject;
    }

    /**
     * @throws Exception
     */
    private function getVersion(): Version
    {
        $version = new Version();

        if ($this->matches->containsKey(self::PG_PREFIX)) {
            $version->setPrefix($this->matches->get('prefix'));
        }

        $defaultValue = 0;
        if ($this->matches->containsKey(self::PG_V_MAJOR)) {
            $version->setMajor($this->matches->getIntValue(
                self::PG_V_MAJOR,
                $defaultValue
            ));
        }

        if ($this->matches->containsKey(self::PG_V_MINOR)) {
            $version->setMinor($this->matches->getIntValue(
                self::PG_V_MINOR,
                $defaultValue
            ));
        }

        if ($this->matches->containsKey(self::PG_V_PATCH)) {
            $version->setPatch($this->matches->getIntValue(
                self::PG_V_PATCH,
                $defaultValue
            ));
        }

        if ($this->matches->containsKey(self::PG_V_PRE_RELEASE)) {
            $type = $this->matches->get(self::PG_V_PRE_RELEASE);
            $version->enablePreRelease($type);

            if ($this->matches->containsKey(self::PG_V_PRE_RELEASE_VERSION)) {
                $version->getPreReleaseComponent($type)->setValue(
                    $this->matches->getIntValue(
                        self::PG_V_PRE_RELEASE_VERSION,
                        $defaultValue
                    )
                );
            }
        }

        return $version;
    }
}
