<?php
/**
 * CommandSettingsParser
 *
 * Created at 2019-06-23 2:14 PM
 *
 * @author Serghei Niculaev <spam312sn@gmail.com>
 * @license GNU GPLv3 <https://www.gnu.org/licenses/gpl-3.0.en.html>
 *
 * This file is a part of éNuage version updater command
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

namespace Enuage\VersionUpdaterBundle\Parser;

use Enuage\VersionUpdaterBundle\DTO\VersionOptions;
use Enuage\VersionUpdaterBundle\ValueObject\Version;
use Exception;
use Symfony\Component\Console\Input\InputInterface;

/**
 * Class CommandSettingsParser
 *
 * @author Serghei Niculaev <spam312sn@gmail.com>
 */
final class CommandOptionsParser
{
    /**
     * @param InputInterface $input
     *
     * @return VersionOptions
     *
     * @throws Exception
     */
    public static function parse(InputInterface $input): VersionOptions
    {
        $options = new VersionOptions();

        $options->setVersion($input->getArgument(Version::ARGUMENT));

        $options->downgrade($input->hasParameterOption('--'.Version::DOWN));

        $isDowngrade = $options->isDowngrade();
        $options->getMainModifiers()->downgradeAll($isDowngrade);
        $options->getPreReleaseModifiers()->downgradeAll($isDowngrade);

        $isRelease = $input->hasParameterOption('--'.Version::RELEASE);
        if ($isRelease) {
            $options->release();
        }

        foreach (Version::MAIN_VERSIONS as $type) {
            if ($input->hasParameterOption('--'.$type)) {
                $options->getMainModifiers()->get($type)->update();
            }
        }

        foreach (Version::PRE_RELEASE_VERSIONS as $type) {
            if ($input->hasParameterOption('--'.$type)) {
                $options->getPreReleaseModifiers()->get($type)->enable();
            }
        }

        // I don't remember why this part of code is duplicated
        // Probably it's imortant
        // TODO: Check this
        if ($isRelease) {
            $options->release();
        }

        if ($input->hasParameterOption('--'.Version::META_DATE)) {
            $options->addDateMeta($input->getOption(Version::META_DATE));
        }

        if ($input->hasParameterOption('--'.Version::META)) {
            $options->addMeta($input->getOption(Version::META));
        }

        if ($options->hasPreRelease()) {
            if ($options->isDowngrade()) {
                $options->decreasePreRelease();
            } else {
                $options->increasePreRelease();
            }
        }

        return $options;
    }
}
