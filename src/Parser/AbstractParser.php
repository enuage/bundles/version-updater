<?php
/**
 * AbstractParser
 *
 * Created at 2019-06-23 12:40 AM
 *
 * @author Serghei Niculaev <spam312sn@gmail.com>
 * @license GNU GPLv3 <https://www.gnu.org/licenses/gpl-3.0.en.html>
 *
 * This file is a part of éNuage version updater command
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

namespace Enuage\VersionUpdaterBundle\Parser;

use Enuage\Type\AdvancedArrayObject;
use Enuage\VersionUpdaterBundle\ValueObject\Version;

/**
 * Class AbstractParser
 *
 * @author Serghei Niculaev <spam312sn@gmail.com>
 */
abstract class AbstractParser
{
    public const PG_PREFIX = 'prefix';
    public const PG_V_MAJOR = 'majorVersion';
    public const PG_V_MINOR = 'minorVersion';
    public const PG_V_PATCH = 'patchVersion';
    public const PG_V_PRE_RELEASE = 'preRelease';
    public const PG_V_PRE_RELEASE_VERSION = 'preReleaseVersion';

    public const VERSION_PATTERN = '(?>'.
    '(?<' . self::PG_PREFIX . '>[a-zA-Z]+)?'.
    '(?<' . self::PG_V_MAJOR . '>\d+)\.?'.
    '(?<' . self::PG_V_MINOR . '>\d*)\.?'.
    '(?<' . self::PG_V_PATCH . '>\d*)'.
    '(?>\-(?<' . self::PG_V_PRE_RELEASE . '>alpha|beta|rc)'.
    '(?>\.(?<' . self::PG_V_PRE_RELEASE_VERSION . '>\d+))?)?'.
    '(?>\+[a-zA-Z\d]+)*'. // Metadata isn't captured
    ')';

    /** @var string */
    protected $pattern;

    /** @var AdvancedArrayObject */
    protected $matches;

    public function __construct()
    {
        $this->matches = new AdvancedArrayObject();
    }

    abstract public function parse(): Version;

    public function getMatches(): AdvancedArrayObject
    {
        return $this->matches;
    }

    public function getPattern(): string
    {
        return $this->pattern;
    }

    protected function setPattern(string $pattern): void
    {
        $this->pattern = $pattern;
    }

    /**
     * @param array|AdvancedArrayObject $matches
     */
    protected function cloneMatches($matches): AbstractParser
    {
        if (is_array($matches)) {
            $this->matches = new AdvancedArrayObject($matches);
        }

        if ($matches instanceof AdvancedArrayObject) {
            $this->matches = $matches;
        }

        return $this;
    }
}
