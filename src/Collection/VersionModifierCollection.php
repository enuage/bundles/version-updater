<?php
/**
 * VersionModifierCollection
 *
 * Created at 2019-06-29 11:57 PM
 *
 * @author Serghei Niculaev <spam312sn@gmail.com>
 * @license GNU GPLv3 <https://www.gnu.org/licenses/gpl-3.0.en.html>
 *
 * This file is a part of éNuage version updater command
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

namespace Enuage\VersionUpdaterBundle\Collection;

use Enuage\Type\AdvancedArrayObject;
use Enuage\VersionUpdaterBundle\ValueObject\VersionModifier;
use Exception;

/**
 * Class VersionModifierCollection
 *
 * @author Serghei Niculaev <spam312sn@gmail.com>
 *
 * @method VersionModifier get($key, $default = null)
 */
class VersionModifierCollection extends AdvancedArrayObject
{
    public const DISABLE_ALL = false;
    public const ENABLE_ALL = true;

    public function __construct(
        array $types = [],
        bool $enableAll = self::DISABLE_ALL
    ) {
        parent::__construct();

        foreach ($types as $type) {
            $this->set($type, new VersionModifier(true === $enableAll));
        }
    }

    /**
     * @throws Exception
     */
    public function enable(string $type): void
    {
        $this->get($type)->enable();
    }

    /**
     * @throws Exception
     */
    public function decrease(string $type): void
    {
        $this->get($type)->setDowngrade(true)->update();
    }

    /**
     * @throws Exception
     */
    public function increase(string $type): void
    {
        $this->get($type)->setDowngrade(false)->update();
    }

    /**
     * @throws Exception
     */
    public function downgradeAll(bool $downgrade): self
    {
        /** @var VersionModifier $type */
        foreach ($this->getIterator() as $type) {
            $type->setDowngrade($downgrade);
        }

        return $this;
    }
}
