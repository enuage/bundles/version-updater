<?php
/**
 * Version
 *
 * Created at 2019-06-22 12:26 AM
 *
 * @author Serghei Niculaev <spam312sn@gmail.com>
 * @license GNU GPLv3 <https://www.gnu.org/licenses/gpl-3.0.en.html>
 *
 * This file is a part of éNuage version updater command
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

namespace Enuage\VersionUpdaterBundle\ValueObject;

use Enuage\Type\AdvancedArrayObject;
use Enuage\VersionUpdaterBundle\Collection\VersionComponentsCollection;
use Exception;

/**
 * Class Version
 *
 * @author Serghei Niculaev <spam312sn@gmail.com>
 */
class Version
{
    public const ARGUMENT = 'version';

    public const MAJOR = 'major';
    public const MINOR = 'minor';
    public const PATCH = 'patch';

    public const MAIN_VERSIONS = [
        self::MAJOR,
        self::MINOR,
        self::PATCH,
    ];

    public const ALPHA = 'alpha';
    public const BETA = 'beta';
    public const RELEASE_CANDIDATE = 'rc';

    public const DOWN = 'down';
    public const RELEASE = 'release';

    public const PRE_RELEASE_VERSIONS = [
        self::ALPHA,
        self::BETA,
        self::RELEASE_CANDIDATE,
    ];

    public const META = 'meta';
    public const META_DATE = 'date';

    /** @var string|null */
    private $prefix;

    /** @var VersionComponentsCollection */
    private $mainComponents;

    /** @var VersionComponentsCollection */
    private $preReleaseComponents;

    /** @var AdvancedArrayObject */
    private $metaComponents;

    public function __construct()
    {
        $this->mainComponents = new VersionComponentsCollection(
            self::MAIN_VERSIONS
        );

        $this->preReleaseComponents = new VersionComponentsCollection(
            self::PRE_RELEASE_VERSIONS,
            VersionComponentsCollection::DISABLE_ALL
        );

        $this->metaComponents = new AdvancedArrayObject();
    }

    public function getPrefix(): ?string
    {
        return $this->prefix;
    }

    public function setPrefix(string $value): self
    {
        $this->prefix = $value;

        return $this;
    }

    /**
     * @throws Exception
     */
    public function getMainVersion(string $type): ?int
    {
        return $this->getMainComponentValue($type);
    }

    /**
     * @throws Exception
     */
    private function getMainComponentValue(string $type): int
    {
        return $this->getMainComponents()->get($type)->getValue();
    }

    public function getMainComponents(): VersionComponentsCollection
    {
        return $this->mainComponents;
    }

    /**
     * @throws Exception
     */
    public function setMainVersion(string $type, int $value): self
    {
        $this->mainComponents->set($type, $value);

        return $this;
    }

    /**
     * @throws Exception
     */
    public function getMajor(): int
    {
        return $this->getMainComponentValue(self::MAJOR);
    }

    /**
     * @throws Exception
     */
    public function setMajor(int $value): self
    {
        $this->mainComponents->set(self::MAJOR, $value);

        return $this;
    }

    /**
     * @throws Exception
     */
    public function getMinor(): int
    {
        return $this->getMainComponentValue(self::MINOR);
    }

    /**
     * @throws Exception
     */
    public function setMinor(int $value): self
    {
        $this->mainComponents->set(self::MINOR, $value);

        return $this;
    }

    /**
     * @throws Exception
     */
    public function getPatch(): int
    {
        return $this->getMainComponentValue(self::PATCH);
    }

    /**
     * @throws Exception
     */
    public function setPatch(int $value): self
    {
        $this->mainComponents->set(self::PATCH, $value);

        return $this;
    }

    /**
     * @throws Exception
     */
    public function clearPreRelease(): self
    {
        $this->preReleaseComponents->disableAll();

        return $this;
    }

    /**
     * @throws Exception
     */
    public function enablePreRelease(string $type): self
    {
        $this->getPreReleaseComponent($type)->setEnabled(true);

        return $this;
    }

    /**
     * @throws Exception
     */
    public function getPreReleaseComponent(string $type): VersionComponent
    {
        return $this->preReleaseComponents->get($type);
    }

    /**
     * @throws Exception
     */
    public function getPreReleaseVersion(): ?int
    {
        return $this->getPreReleaseComponent($this->getPreRelease())->getValue();
    }

    /**
     * @throws Exception
     */
    public function getPreRelease(): ?string
    {
        /** @var VersionComponent $component */
        foreach ($this->preReleaseComponents->getIterator() as $type => $component) {
            if ($component->isEnabled()) {
                return $type;
            }
        }

        return null;
    }

    /**
     * @throws Exception
     */
    public function setPreReleaseVersion(string $type, int $value): void
    {
        $this->getPreReleaseComponent($type)->setValue($value);
    }

    public function getMetaComponents(): AdvancedArrayObject
    {
        return $this->metaComponents;
    }

    public function setMetaComponents(AdvancedArrayObject $metaComponents): void
    {
        $this->metaComponents = $metaComponents;
    }

    public function implodeMainComponents(): string
    {
        return implode('.', $this->getMainComponents()->toArray());
    }
}
