<?php
/**
 * MainVersion
 *
 * Created at 2019-06-29 11:48 PM
 *
 * @author Serghei Niculaev <spam312sn@gmail.com>
 * @license GNU GPLv3 <https://www.gnu.org/licenses/gpl-3.0.en.html>
 *
 * This file is a part of éNuage version updater command
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

namespace Enuage\VersionUpdaterBundle\ValueObject;

/**
 * Class VersionModifier
 *
 * @author Serghei Niculaev <spam312sn@gmail.com>
 */
class VersionModifier
{
    private const MAXIMAL_MODIFIER_VALUE = 1;
    private const IGNORED_MODIFIER_VALUE = 0;
    private const MINIMAL_MODIFIER_VALUE = -1;

    /** @var int */
    private $modifier = self::IGNORED_MODIFIER_VALUE;

    /** @var bool */
    private $downgrade = false;

    /** @var bool */
    private $enabled;

    public function __construct(bool $isEnabled = false)
    {
        $this->enabled = $isEnabled;
    }

    public function update(): self
    {
        if ($this->isEnabled()) {
            if ($this->isDowngrade()) {
                if (self::MINIMAL_MODIFIER_VALUE < $this->modifier) {
                    $this->modifier--;
                }
            } elseif (self::MAXIMAL_MODIFIER_VALUE > $this->modifier) {
                $this->modifier++;
            }
        }

        return $this;
    }

    public function isEnabled(): bool
    {
        return $this->enabled;
    }

    public function isDowngrade(): bool
    {
        return $this->downgrade;
    }

    public function setDowngrade(bool $downgrade): self
    {
        $this->downgrade = $downgrade;

        return $this;
    }

    public function isUpdated(): bool
    {
        return self::IGNORED_MODIFIER_VALUE !== $this->modifier;
    }

    public function getValue(): int
    {
        return $this->modifier;
    }

    public function enable(): void
    {
        $this->enabled = true;
    }

    public function disable(): void
    {
        $this->enabled = false;
    }
}
