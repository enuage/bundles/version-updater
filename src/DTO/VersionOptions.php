<?php
/**
 * VersionOptions
 *
 * Created at 2019-06-23 2:41 PM
 *
 * @author Serghei Niculaev <spam312sn@gmail.com>
 * @license GNU GPLv3 <https://www.gnu.org/licenses/gpl-3.0.en.html>
 *
 * This file is a part of éNuage version updater command
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

namespace Enuage\VersionUpdaterBundle\DTO;

use DateTime;
use Enuage\Type\AdvancedArrayObject;
use Enuage\VersionUpdaterBundle\Collection\VersionModifierCollection;
use Enuage\VersionUpdaterBundle\Helper\Type\BooleanType;
use Enuage\VersionUpdaterBundle\ValueObject\MetaComponent;
use Enuage\VersionUpdaterBundle\ValueObject\Version;
use Enuage\VersionUpdaterBundle\ValueObject\VersionModifier;
use Exception;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * Class VersionOptions
 *
 * @author Serghei Niculaev <spam312sn@gmail.com>
 */
class VersionOptions
{
    /** @var null|string */
    private $version;

    /** @var VersionModifierCollection */
    private $mainModifiers;

    /** @var VersionModifierCollection */
    private $preReleaseModifiers;

    /** @var bool */
    private $down = false;

    /** @var bool */
    private $release = false;

    /** @var VersionModifier */
    private $preReleaseVersionModifier;

    /** @var AdvancedArrayObject */
    private $metaComponents;

    /** @var string */
    private $gitVersion;

    /** @var string */
    private $prefix;

    public function __construct()
    {
        $this->mainModifiers = new VersionModifierCollection(
            Version::MAIN_VERSIONS,
            VersionModifierCollection::ENABLE_ALL
        );
        $this->preReleaseModifiers = new VersionModifierCollection(
            Version::PRE_RELEASE_VERSIONS
        );
        $this->metaComponents = new AdvancedArrayObject();
        $this->preReleaseVersionModifier = new VersionModifier();
        $this->preReleaseVersionModifier->enable();
    }

    /**
     * @throws Exception
     */
    public function addDateMeta(string $format = null): VersionOptions
    {
        $metaComponent = new MetaComponent();
        $metaComponent->setType(MetaComponent::TYPE_DATETIME);
        $metaComponent->setValue(new DateTime());
        $metaComponent->setFormat($format ?? 'c');

        $this->metaComponents->set(Version::META_DATE, $metaComponent);

        return $this;
    }

    /**
     * @throws Exception
     */
    public function addMeta(string $value = null): VersionOptions
    {
        $metaComponent = new MetaComponent();
        $metaComponent->setValue($value);

        $this->metaComponents->set(Version::META, $metaComponent);

        return $this;
    }

    /**
     * @throws Exception
     */
    public function increaseMajor(): self
    {
        $this->getMainModifiers()->increase(Version::MAJOR);

        return $this;
    }

    public function getMainModifiers(): VersionModifierCollection
    {
        return $this->mainModifiers;
    }

    /**
     * @throws Exception
     */
    public function decreaseMajor(): self
    {
        $this->getMainModifiers()->decrease(Version::MAJOR);

        return $this;
    }

    /**
     * @throws Exception
     */
    public function increaseMinor(): self
    {
        $this->getMainModifiers()->increase(Version::MINOR);

        return $this;
    }

    /**
     * @throws Exception
     */
    public function decreaseMinor(): self
    {
        $this->getMainModifiers()->decrease(Version::MINOR);

        return $this;
    }

    /**
     * @throws Exception
     */
    public function increasePatch(): self
    {
        $this->getMainModifiers()->increase(Version::PATCH);

        return $this;
    }

    /**
     * @throws Exception
     */
    public function decreasePatch(): self
    {
        $this->getMainModifiers()->decrease(Version::PATCH);

        return $this;
    }

    /**
     * @throws Exception
     */
    public function updateAlpha(): self
    {
        $this->getPreReleaseModifiers()->enable(Version::ALPHA);

        return $this;
    }

    public function getPreReleaseModifiers(): VersionModifierCollection
    {
        return $this->preReleaseModifiers;
    }

    /**
     * @throws Exception
     */
    public function updateBeta(): self
    {
        $this->getPreReleaseModifiers()->enable(Version::BETA);

        return $this;
    }

    /**
     * @throws Exception
     */
    public function updateReleaseCandidate(): self
    {
        $this->getPreReleaseModifiers()->enable(Version::RELEASE_CANDIDATE);

        return $this;
    }

    public function hasVersion(): bool
    {
        return null !== $this->version;
    }

    public function downgrade(bool $value = true): self
    {
        $this->down = $value;

        return $this;
    }

    /**
     * @throws Exception
     */
    public function getMainModifier(string $type): ?VersionModifier
    {
        return $this->getMainModifiers()->get($type);
    }

    public function increasePreRelease(): self
    {
        $this->getPreReleaseVersionModifier()->setDowngrade(false)->update();

        return $this;
    }

    public function getPreReleaseVersionModifier(): VersionModifier
    {
        return $this->preReleaseVersionModifier;
    }

    public function decreasePreRelease(): self
    {
        $this->getPreReleaseVersionModifier()->setDowngrade(true)->update();

        return $this;
    }

    /**
     * @throws Exception
     */
    public function hasPreRelease(): bool
    {
        foreach ($this->getPreReleaseModifiers()->getIterator() as $type) {
            if ($type->isEnabled()) {
                return true;
            }
        }

        return false;
    }

    /**
     * @throws Exception
     */
    public function release(): void
    {
        $this->release = true;

        /** @var VersionModifier $type */
        foreach ($this->getPreReleaseModifiers()->getIterator() as $type) {
            $type->disable();
        }
    }

    /**
     * @throws Exception
     */
    public function consoleDebug(SymfonyStyle $io): void
    {
        $rows[] = ['Set version', $this->getVersion() ?? 'N\\A'];
        foreach (Version::MAIN_VERSIONS as $version) {
            $rows[] = [
                'Update '.$version,
                BooleanType::toShortStatement(
                    $this->isMainVersionUpdated($version)
                ),
            ];
        }

        foreach (Version::PRE_RELEASE_VERSIONS as $version) {
            $rows[] = [
                'Update '.$version,
                BooleanType::toShortStatement(
                    $this->getPreReleaseModifiers()->get($version)->isEnabled()
                ),
            ];
        }

        $rows[] = [
            'Downgrade',
            BooleanType::toShortStatement($this->isDowngrade()),
        ];
        $rows[] = [
            'Is release',
            BooleanType::toShortStatement($this->isRelease()),
        ];

        foreach ([Version::META_DATE, Version::META] as $meta) {
            $rows[] = [
                'Add '.$meta,
                BooleanType::toShortStatement(
                    $this->getMetaComponents()->containsKey($meta)
                ),
            ];
        }

        $rows[] = ['Git version', $this->getGitVersion() ?? 'N\\A'];

        $io->table(['Option', 'Value'], $rows);
    }

    public function getVersion(): ?string
    {
        return $this->version;
    }

    public function setVersion(?string $version = null): void
    {
        $this->version = $version;
    }

    /**
     * @throws Exception
     */
    public function isMainVersionUpdated(string $name): bool
    {
        return $this->getMainModifiers()->get($name)->isUpdated();
    }

    public function isDowngrade(): bool
    {
        return $this->down || $this->isPreReleaseDowngrade();
    }

    private function isPreReleaseDowngrade(): bool
    {
        return $this->getPreReleaseVersionModifier()->isDowngrade();
    }

    public function isRelease(): bool
    {
        return $this->release;
    }

    public function getMetaComponents(): AdvancedArrayObject
    {
        return $this->metaComponents;
    }

    public function getGitVersion(): ?string
    {
        return $this->gitVersion;
    }

    public function setGitVersion(string $gitVersion): void
    {
        $this->gitVersion = $gitVersion;
    }

    public function getPrefix(): ?string
    {
        return $this->prefix;
    }

    public function setPrefix(?string $prefix): void
    {
        $this->prefix = $prefix;
    }
}
